#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "waypoint.h"
#include "enemy.h"
#include "bullet.h"
#include "audioplayer.h"
#include "plistreader.h"
#include "windows.h"
#include <QPainter>
#include <QMouseEvent>
#include <QtGlobal>
#include <QMessageBox>
#include <QTimer>
#include <QXmlStreamReader>
#include <QtDebug>
#include <QIcon>
#include <QPixmap>
#include <QTime>
#include <QApplication>
#include <QProcess>
#include <QSplashScreen>
#include <QGraphicsWidget>
#include <QWidget>
#include <QScreen>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QDir>
#include <QVector>
#include <iostream>
#include <QMessageBox>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QGuiApplication>
#include <QLabel>
#include <QPushButton>
#include <QTimer>
#include <QList>
#include <QPalette>
#include <QLabel>
#include <QMediaPlayer>
#include <QGridLayout>
#include <QString>
QFont font1("Times New Roman",20,QFont::Bold,false);
static const int TowerCost = 300;
int cnt=2;
MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
	, m_waves(0)
    , m_playerHp(1)
    , m_playerGold(1000)
	, m_gameEnded(false)
	, m_gameWin(false)
{
    restart_flag=0;
    ui->setupUi(this);
    this->setWindowTitle(QStringLiteral("保卫萝卜"));
    this->setWindowIcon(QIcon(":/image/0.jpg"));
    this->setMinimumSize(1440,840);
    this->setMaximumSize(1440,840);
    preLoadWavesInfo(1);
    loadTowerPositions();
    addWayPoints();
    m_audioPlayer = new AudioPlayer(this);
    m_audioPlayer->startBGM();
    this->pauseButton = new QPushButton(QStringLiteral("暂停"),this);
    this->pauseButton->move(9,50);
    pauseButton->setStyleSheet(tr("background-image: url(:/image/07.PNG);"));
    QObject::connect(this->pauseButton,SIGNAL(clicked(bool)),this,SLOT(pause()));
    this->exitButton_1 = new QPushButton(QStringLiteral("结束"),this);
    this->exitButton_1->move(9,287);
    exitButton_1->setStyleSheet(tr("background-image: url(:/image/07.PNG);"));
    QObject::connect(this->exitButton_1,SIGNAL(clicked(bool)),this,SLOT(close()));
    this->replay = new QPushButton(QStringLiteral("重玩"),this);
    this->replay->move(850,647);
    replay->setStyleSheet(tr("background-image: url(:/image/07.PNG);"));
    QObject::connect(this->replay,SIGNAL(clicked(bool)),this,SLOT(gamereStart()));
    this->screenShotButton = new QPushButton(QStringLiteral("截图"),this);
    this->screenShotButton->move(1090,647);
    screenShotButton->setStyleSheet(tr("background-image: url(:/image/07.PNG);"));
    QObject::connect(this->screenShotButton,SIGNAL(clicked(bool)),this,SLOT(screenShotSlot()));
    this->startMenu();
}
MainWindow::~MainWindow()
{
	delete ui;
}
void MainWindow::startMenu()
{
    option_flag=0;
    if(!restart_flag)
    {
        QSplashScreen* splash1 = new QSplashScreen;
        QSplashScreen* splash2= new QSplashScreen;
        QSplashScreen* splash3 = new QSplashScreen;
        splash1->setPixmap(QPixmap(":/image/3.jpg"));
            splash1->show();
            Sleep(1000);
            splash2->setPixmap(QPixmap(":/image/4.jpg"));
            splash2->show();
            Sleep(1000);
            splash3->setPixmap(QPixmap(":/image/t.jpg"));
            splash3->show();
            Sleep(1000);
            delete splash1;
            delete splash2;
            delete splash3;
    }

    this->pauseButton->hide();
    this->exitButton_1->hide();
    this->replay->hide();
    this->screenShotButton->hide();
    this->startButton = new QPushButton("开始",this);
    this->optionButton = new QPushButton("选关",this);
    this->helpButton = new QPushButton("帮助",this);
    this->exitButton = new QPushButton("退出",this);

    this->startButton->setGeometry(660,280,100,50);
    startButton->setStyleSheet(tr("background-image: url(:/image/06.PNG);"));
    this->optionButton->setGeometry(660,345,100,50);
    optionButton->setStyleSheet(tr("background-image: url(:/image/06.PNG);"));
    this->helpButton->setGeometry(660,410,100,50);
    helpButton->setStyleSheet(tr("background-image: url(:/image/06.PNG);"));
     this->exitButton->setGeometry(660,475,100,50);
    exitButton->setStyleSheet(tr("background-image: url(:/image/06.PNG);"));
    start_flag=1;
    connect(startButton, SIGNAL(clicked(bool)), this, SLOT(gameStart()));
    connect(optionButton, SIGNAL(clicked(bool)), this, SLOT(change_interface()));
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateMap()));
    timer->start(30);
    QObject::connect(exitButton,SIGNAL(clicked(bool)),this,SLOT(close()));

    return;
    // 设置300ms后游戏启动
}
void MainWindow::loadTowerPositions()
{
	QFile file(":/config/TowersPosition.plist");
	if (!file.open(QFile::ReadOnly | QFile::Text))
	{
		QMessageBox::warning(this, "TowerDefense", "Cannot Open TowersPosition.plist");
		return;
	}

	PListReader reader;
	reader.read(&file);

	QList<QVariant> array = reader.data();
	foreach (QVariant dict, array)
	{
		QMap<QString, QVariant> point = dict.toMap();
		int x = point.value("x").toInt();
		int y = point.value("y").toInt();
		m_towerPositionsList.push_back(QPoint(x, y));
	}

	file.close();
}
void MainWindow::paintEvent(QPaintEvent *)
{
    if(option_flag)
        {
            QPixmap level1(":/image/level1.png");
            QPixmap level2(":/image/level2.png");
            QPixmap level3(":/image/level3.png");
            QPixmap level4(":/image/level4.png");
            QPainter levelpainter(this);
            levelpainter.drawPixmap(0,0,level1);
            levelpainter.drawPixmap(720,0,level2);
            levelpainter.drawPixmap(0,420,level3);
            levelpainter.drawPixmap(720,420,level4);
            return;
        }
    if(start_flag)
    {
        QPixmap start_picture(":/image/g.jpg");
        QPainter startpainter(this);
        startpainter.drawPixmap(0,0,start_picture);
        this->startButton ->show();
        this->optionButton ->show();
        this->helpButton ->show();
        this->exitButton ->show();
        if(restart_flag)
                {
                    m_waves=0;
                    m_playerGold=1000;
                    m_wavesInfo.clear();
                    m_towerPositionsList.clear();
                    m_towersList.clear();
                    m_wayPointsList.clear();
                    m_enemyList.clear();
                    m_bulletList.clear();
                    preLoadWavesInfo(1);
                    loadTowerPositions();
                    addWayPoints();
                    m_gameEnded=0;
                    m_gameWin=0;
                    m_playerHp=1;
                }

        return;
    }
	if (m_gameEnded || m_gameWin)
	{
        this->pauseButton->hide();
        this->exitButton_1->hide();
        this->replay->hide();
        this->screenShotButton->hide();
        QPainter painter(this);
        QPixmap pix;
        if(m_gameWin)
        {

            pix.load(":/image/25.jpg");
            painter.drawPixmap(0,0,1440,840,pix);

}
        if(m_gameEnded)
        {
            pix.load(":/image/26.jpg");
            painter.drawPixmap(0,0,1440,840,pix);


        }

       return ;
	}

    QPixmap cachePix(":/image/Bg.png");
        QPainter cachePainter(&cachePix);
        foreach (const Tower *tower, m_towersList)
            tower->draw(&cachePainter);
        foreach (const WayPoint *wayPoint, m_wayPointsList)
            wayPoint->draw(&cachePainter);
        foreach (const Enemy *enemy, m_enemyList)
            enemy->draw(&cachePainter);
        foreach (const Bullet *bullet, m_bulletList)
            bullet->draw(&cachePainter);
        QPainter painter(this);
        painter.drawPixmap(0, 0, cachePix);
        QPixmap Pix1;
        QPixmap Pix2;
        if(flag==1)
        {
            if(m_playerGold<220)
                Pix1=QPixmap(":/image/tower1_nopayment.png");
            else
                Pix1=QPixmap(":/image/tower1_payment.png");
            if(m_playerGold<260)
                Pix2=QPixmap(":/image/tower2_nopayment.png");
            else
                Pix2=QPixmap(":/image/tower2_payment.png");
            QPainter mypainter(this);
            int x=temp.rx();
            int y=temp.ry();
            mypainter.drawPixmap(x-110,y-120,Pix1);
            mypainter.drawPixmap(x+50,y-120,Pix2);
        }
        QPixmap coinbar=QPixmap(":/image/bar.png");
        QPainter mybar(this);
        mybar.drawPixmap(740,785,coinbar);
        drawPlayerGold(&mybar);
        drawHP(&mybar);
        drawWave(&mybar);
}
void MainWindow::mousePressEvent(QMouseEvent *event)
{
    QPoint mypoint;
    QPoint pressPos = event->pos();
   auto it = m_towerPositionsList.begin();//auto 自动匹配类型
    if(option_flag)
        {
            if(pressPos.rx()>=0&&pressPos.rx()<=720&&pressPos.ry()>=0&&pressPos.ry()<=420)
                preLoadWavesInfo(1);
            if(pressPos.rx()>720&&pressPos.rx()<=1440&&pressPos.ry()>=0&&pressPos.ry()<=420)
                preLoadWavesInfo(2);
            if(pressPos.rx()>=0&&pressPos.rx()<=720&&pressPos.ry()>420&&pressPos.ry()<=840)
                preLoadWavesInfo(3);;
            if(pressPos.rx()>720&&pressPos.rx()<=1440&&pressPos.ry()>420&&pressPos.ry()<=840)
                preLoadWavesInfo(4);
            option_flag=0;
            gameStart();
            return;
        }

    if(flag==1)
    {
        for(int i=0;i<times;i++)
        {
            it++;
        }
        mypoint.setX(pressPos.rx()+110-40);
        mypoint.setY(pressPos.ry()+120-40);
        if(it->containPoint(mypoint))
        {
            Tower *tower = new Tower(QString::fromLocal8Bit("weapon1"),it->centerPos(), this);
            if(m_playerGold<tower->getcost())
            {
                update();
                return;
            }
            m_playerGold -= tower->getcost();
            m_towersList.push_back(tower);
            flag=0;
            m_audioPlayer->playSound(TowerPlaceSound);
            it->setHasTower();
            update();
            return;
        }
        mypoint.setX(pressPos.rx()-50-40);
        mypoint.setY(pressPos.ry()+120-40);
        if(it->containPoint(mypoint))
        {
            Tower *tower = new Tower(QString::fromLocal8Bit("weapon2"),it->centerPos(), this);
            if(m_playerGold<tower->getcost())
            {
                update();
                return;
            }
            m_playerGold -= tower->getcost();
            m_towersList.push_back(tower);
            flag=0;
            m_audioPlayer->playSound(TowerPlaceSound);
            it->setHasTower();
            update();
            return;
        }
        flag=0;
        update();
        return;
    }
    times=0;
    while (it != m_towerPositionsList.end())
    {
        if (it->containPoint(pressPos) && !it->hasTower())
        {
            flag=1;
            temp=it->centerPos();

            break;
        }
        ++times;
        ++it;
    }
}


bool MainWindow::canBuyTower() const
{
    if (m_playerGold >= TowerCost)
		return true;
	return false;
}

void MainWindow::drawWave(QPainter *painter)
{
    painter->setPen(QPen(Qt::white));
    painter->setFont(font1);
    painter->drawText(1340, 825, QString("%1").arg(m_waves + 1));
}

void MainWindow::drawHP(QPainter *painter)
{
    painter->setPen(QPen(Qt::white));
    painter->setFont(font1);
    painter->drawText(1080, 825,QString("%1").arg(m_playerHp));
}

void MainWindow::drawPlayerGold(QPainter *painter)
{
    painter->setPen(QPen(Qt::white));
    painter->setFont(font1);
    painter->drawText(820, 825, QString("%1").arg(m_playerGold));
}

void MainWindow::doGameOver()
{
    if (!m_gameEnded)
	{
		m_gameEnded = true;
        start_flag=0;
        repaint();
        Sleep(1500);
        restart_flag=1;
        start_flag=1;
        repaint();
        // 此处应该切换场景到结束场景
		// 暂时以打印替代,见paintEvent处理
	}

}

void MainWindow::awardGold(int gold)
{
    m_playerGold += 100;
	update();
}

AudioPlayer *MainWindow::audioPlayer() const
{
	return m_audioPlayer;
}

void MainWindow::addWayPoints()
{
    WayPoint *wayPoint11 = new WayPoint(QPoint(1260, 540));
    WayPoint *wayPoint10 = new WayPoint(QPoint(660, 540));
    wayPoint10->setNextWayPoint(wayPoint11);
    WayPoint *wayPoint9 = new WayPoint(QPoint(660, 642));
    wayPoint9->setNextWayPoint(wayPoint10);
    WayPoint *wayPoint8 = new WayPoint(QPoint(420, 642));
    wayPoint8->setNextWayPoint(wayPoint9);
    WayPoint *wayPoint7 = new WayPoint(QPoint(420, 295));
    wayPoint7->setNextWayPoint(wayPoint8);
    WayPoint *wayPoint6 = new WayPoint(QPoint(175, 295));
    wayPoint6->setNextWayPoint(wayPoint7);
    WayPoint *wayPoint5 = new WayPoint(QPoint(175, 180));
    wayPoint5->setNextWayPoint(wayPoint6);
    WayPoint *wayPoint4 = new WayPoint(QPoint(780, 175));
    wayPoint4->setNextWayPoint(wayPoint5);
    WayPoint *wayPoint3 = new WayPoint(QPoint(780, 325));
    wayPoint3->setNextWayPoint(wayPoint4);
    WayPoint *wayPoint2 = new WayPoint(QPoint(1260, 325));
    wayPoint2->setNextWayPoint(wayPoint3);
    WayPoint *wayPoint1 = new WayPoint(QPoint(1260, 205));
    wayPoint1->setNextWayPoint(wayPoint2);

    m_wayPointsList.push_back(wayPoint11);
    m_wayPointsList.push_back(wayPoint10);
    m_wayPointsList.push_back(wayPoint9);
    m_wayPointsList.push_back(wayPoint8);
    m_wayPointsList.push_back(wayPoint7);
    m_wayPointsList.push_back(wayPoint6);
    m_wayPointsList.push_back(wayPoint5);
    m_wayPointsList.push_back(wayPoint4);
    m_wayPointsList.push_back(wayPoint3);
    m_wayPointsList.push_back(wayPoint2);
    m_wayPointsList.push_back(wayPoint1);

}

void MainWindow::getHpDamage(int damage/* = 1*/)
{
	m_audioPlayer->playSound(LifeLoseSound);
	m_playerHp -= damage;
	if (m_playerHp <= 0)
		doGameOver();

}

void MainWindow::removedEnemy(Enemy *enemy)
{
	Q_ASSERT(enemy);

	m_enemyList.removeOne(enemy);
	delete enemy;

	if (m_enemyList.empty())
	{
		++m_waves;
		if (!loadWave())
		{
            m_gameWin = true;
            start_flag=0;
            repaint();
            Sleep(1500);
            restart_flag=1;
            start_flag=1;
            repaint();

			// 游戏胜利转到游戏胜利场景
			// 这里暂时以打印处理
		}
	}
}

void MainWindow::removedBullet(Bullet *bullet)
{
	Q_ASSERT(bullet);

	m_bulletList.removeOne(bullet);
	delete bullet;
}

void MainWindow::addBullet(Bullet *bullet)
{
	Q_ASSERT(bullet);

	m_bulletList.push_back(bullet);
}

void MainWindow::updateMap()
{
	foreach (Enemy *enemy, m_enemyList)
		enemy->move();
	foreach (Tower *tower, m_towersList)
		tower->checkEnemyInRange();
	update();
}

void MainWindow::preLoadWavesInfo(int level)
{
    QFile file;
    switch(level)
    {
    case 1:
        file.setFileName(":/config/Waves.plist");
        break;
    case 2:
        file.setFileName(":/config/waves2.plist");
        break;
    case 3:
        file.setFileName(":/config/waves3.plist");
        break;
    case 4:
        file.setFileName(":/config/waves4.plist");
        break;
    }


    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        QMessageBox::warning(this, "TowerDefense", "Cannot Open TowersPosition.plist");
        return;
    }

    PListReader reader;
    reader.read(&file);

    // 获取波数信息
    m_wavesInfo = reader.data();

    file.close();
}


bool MainWindow::loadWave()
{
	if (m_waves >= m_wavesInfo.size())
		return false;

	WayPoint *startWayPoint = m_wayPointsList.back();
	QList<QVariant> curWavesInfo = m_wavesInfo[m_waves].toList();


           for (int i = 0; i < curWavesInfo.size(); ++i)
           {
               QMap<QString, QVariant> dict = curWavesInfo[i].toMap();
               int spawnTime = dict.value("spawnTime").toInt();

               Enemy *enemy=new Enemy(dict.value("data").toString(),startWayPoint,this);

               m_enemyList.push_back(enemy);
               QTimer::singleShot(spawnTime, enemy, SLOT(doActivate()));
           }


	return true;
}

QList<Enemy *> MainWindow::enemyList() const
{
	return m_enemyList;
}

void MainWindow::gameStart()
{
    start_flag=0;
	loadWave();
    this->startButton->hide();
    this->optionButton->hide();
    this->helpButton->hide();
    this->exitButton->hide();
    this->pauseButton->show();
    this->exitButton_1->show();
    this->replay->show();
    this->screenShotButton->show();
}

void MainWindow::screenShotSlot()
{

    QScreen * screen = QGuiApplication::primaryScreen();
    screen->grabWindow(this->winId()).save(QString("D:/screenshot/")+QString::number(cnt-2)+QString(".jpg"),"jpg");
    cnt++;
}
void MainWindow::change_interface()
{
    option_flag=1;
    update();
    this->startButton->hide();
    this->optionButton->hide();
    this->helpButton->hide();
    this->exitButton->hide();
}
void MainWindow::gamereStart()
{
    qApp->closeAllWindows();
    QProcess::startDetached(qApp->applicationFilePath(), QStringList());
}



